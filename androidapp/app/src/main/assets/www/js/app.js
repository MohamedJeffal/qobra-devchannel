// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
var app = angular.module('starter', ['ionic', 'starter.controllers', 'starter.services', 'ionic.utils', 'weatherModule', 'ngSanitize'])

app.run(function($ionicPlatform, $rootScope, $state, AuthService, AUTH_EVENTS) {
   $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });

   $rootScope.$on('$stateChangeStart', function (event, toState, toParams, formState, fromParams) {
       //If the user
       if ('data' in toState && 'authorizedRoles' in toState.data) {
           var authorizedRoles = toState.data.authorizedRoles;
           if (!AuthService.isAuthorized(authorizedRoles)) {
               event.preventDefault();
               $state.go($state.current, {}, {reload: true});
               $rootScope.$broadcast(AUTH_EVENTS.notAuthorized);
           }
       }

       //If the user is not authenticated we send him to the login page
       if (!AuthService.isAuthenticated()) {
           if (toState.name !== 'login') {
               event.preventDefault();
               $state.go('login');
           }
       }
   })
});

app.config(function($stateProvider, $urlRouterProvider, USER_ROLES) {

    $stateProvider.state('home', {
        url: '/home',
        templateUrl: "templates/home.html",
        controller: 'HomeCtrl'
    });

    $stateProvider.state('qrad', {
        url: '/qrad/:id',
        templateUrl: 'templates/qrad.html',
        controller: 'QradCtrl'
    });

    $stateProvider.state('qradSettings', {
        url: '/qrad/:id/settings',
        templateUrl: 'templates/qrad_settings.html',
        controller: 'QradSettingsCtrl'
    });

    $stateProvider.state('settings', {
        url: '/settings',
        templateUrl: 'templates/settings.html',
        controller: 'SettingsCtrl'
    });

    /*$stateProvider.state('admin', {
        url: '/admin',
        templateUrl: 'templates/url.html',
        controller: 'AdminCtrl',
        data: {
            authorizedRoles: [USER_ROLES.admin]
        }
    });*/

    $stateProvider.state('login', {
        url: '/login',
        templateUrl: 'templates/login.html',
        controller: 'LoginCtrl'
    });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/login');


});

