angular.module('starter.controllers')

    .controller('QradSettingsCtrl', function($scope, $state, $stateParams, $http, Qrads, AuthService, API) {
        function initSettingQrad(response) {
            if (response == null) {
                $state.go('home');
            } else {
                $scope.qrad = response;
            }
        }

        $scope.qrad = {};
        Qrads.get($stateParams.id, initSettingQrad);

        $scope.updateQrad = function() {
            $state.go('qrad', { id : $scope.qrad.id });

            var data = {};
            data.friendlyName = $scope.qrad.friendlyName;
            $http.put(API.urlBase + '/qrads/' + $scope.qrad.id, JSON.stringify(data), AuthService.config())
                .then(function(response) {

                }, function(response) {
                    console.log(response);
                });
        };
        $scope.updateColor = function() {

        };
    });