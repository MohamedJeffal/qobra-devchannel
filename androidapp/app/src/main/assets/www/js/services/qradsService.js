function addHexColor(c1, c2) {
    var hexStr = (parseInt(c1, 16) + parseInt(c2, 16)).toString(16);
    while (hexStr.length < 6) { hexStr = '0' + hexStr; } // Zero pad.
    return hexStr;
};

function randomColor() {
    var letters = '0123456789ABCDEF'.split('');
    var color = '#';
    for (var i = 0; i < 6; i++ ) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

angular.module('starter.services', ['ionic.utils'])
    .factory('Qrads', function($http, AuthService, API) {
        var qrads = [];

        function initQrads() {
            for (var i = 0; i < qrads.length; ++i) {
                qrads[i].color = randomColor();
                qrads[i].endColor = randomColor();
            }
        }

        var all = function(callback) {
            if (AuthService.isAuthenticated()) {
                if (qrads.length == 100) {
                    console.log('Fail');
                    callback(qrads);
                } else {
                    $http.get(API.urlBase + '/qrads', AuthService.config())
                        .then(function (response) {
                            qrads = response.data;
                            initQrads();
                            callback(qrads);
                        }, function(response) {
                            //Print error
                            console.log(response);
                            callback([]);
                        });
                }
            }
            return [];
        };

        var get = function(id, callback) {
            $http.get(API.urlBase + '/qrads/' + id)
                .then(function(response) {
                    response.data.color = randomColor();
                    response.data.endColor = randomColor();
                    callback(response.data);
                }, function(response) {
                    callback(null);
                });
        };

        return {
            all: all,
            get: get,
            clear: function() { qrads = []; }
        };
    });