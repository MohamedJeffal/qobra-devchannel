angular.module('starter.controllers')

    .controller('QradSettingsCtrl', function($scope, $state, $stateParams, Qrads, AuthService, API) {
        function initSettingQrad(response) {
            if (response == null) {
                $state.go('home');
            } else {
                $scope.qrad = response;
            }
        }

        $scope.qrad = {};
        Qrads.get($stateParams.id, initSettingQrad);

        $scope.updateQrad = function() {
            $state.go('qrad', { id : $scope.qrad.id });

            Qrads.updateInfoQrad($scope.qrad);
        };
        $scope.updateColor = function() {

        };
    });