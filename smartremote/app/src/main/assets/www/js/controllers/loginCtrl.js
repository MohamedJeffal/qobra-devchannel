angular.module('starter.controllers')

	.controller('LoginCtrl', function($scope, $state, $ionicPopup, AuthService) {
        if (AuthService.isAuthenticated())
            $state.go('home');

        $scope.data = {};
        $scope.data.username = 'pfee-epita';
        $scope.data.password = 'epita-pfee-42';

        $scope.login = function(data) {
            AuthService.login(data.username, data.password).then(function(authenticated) {
                $state.go('home', {}, {reload: true});
                $scope.setCurrentUsername(data.username);
            }, function(err) {
                /*var alertPopup = $ionicPopup.alert({
                    title: 'Login failed!',
                    template: 'Please check your credentials!'
                });*/
            });
        };
    });