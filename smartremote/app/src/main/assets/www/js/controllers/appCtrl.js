angular.module('starter.controllers', [])

    .controller('AppCtrl', function($scope, $state, $ionicPopup, AuthService, AUTH_EVENTS) {
        // With the new view caching in Ionic, Controllers are only called
        // when they are recreated or on app start, instead of every page change.
        // To listen for when this page is active (for example, to refresh data),
        // listen for the $ionicView.enter event:
        //$scope.$on('$ionicView.enter', function(e) {
        //});

        // Form data for the login modal
        $scope.username = AuthService.username();

        $scope.$on(AUTH_EVENTS.notAuthorized, function(event) {
           /*var alertPopup = $ionicPopup.alert({
               title: 'Unauthorized!',
               template: 'You are not allowed to access this resource.'
           });*/
        });

        $scope.$on(AUTH_EVENTS.notAuthenticated, function(event) {
            //AuthService.logout();
            //$state.go('login');
            /*var alertPopup = $ionicPopup.alert({
                title: 'Session Lost!',
                template: 'Sorry, You have to login again.'
            });*/
        });

        $scope.setCurrentUsername = function (name) {
            $scope.username = name;
        };
    });