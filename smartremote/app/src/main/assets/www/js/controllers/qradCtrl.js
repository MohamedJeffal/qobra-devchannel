angular.module('starter.controllers')

    .controller('QradCtrl', function($scope, $stateParams, $state, Qrads, AuthService, API) {
        var userConfig = AuthService.userConfig();

        var min_temp = 16;
        var max_temp = 26;
        var step = 1;
        function initScopeForQrad(Qrad) {
            if (Qrad == null || Qrad == undefined) {
                $state.go('home');
            } else {
                $scope.qrad = Qrad;

                if (Qrad.version == "2.0") {
                    min_temp = userConfig.targetTemperatureV2Min;
                    max_temp = userConfig.targetTemperatureV2Max;
                    step = userConfig.targetTemperatureV2Step;
                } else {
                    min_temp = userConfig.targetTemperatureV1Min;
                    max_temp = userConfig.targetTemperatureV1Max;
                    step = userConfig.targetTemperatureV1Step;
                }

                $scope.Temp_Float = Math.round(($scope.qrad.sensors.ambientTemperature % 1) * 10);
                $scope.Temp_Int = Math.floor($scope.qrad.sensors.ambientTemperature);

                $scope.temp_percent = ($scope.Temp_Int - min_temp) / (max_temp - min_temp) * 100;
                //$scope.qrad.targetTemperature = $scope.Temp_Int;
                $scope.qrad.targetTemperature_css = ($scope.qrad.targetTemperature - min_temp) / (max_temp - min_temp) * 100;

                if ($scope.qrad.targetTemperature_css == 0)
                    $scope.qrad.targetTemperature_css = 5;
                if ($scope.qrad.targetTemperature_css == 100)
                    $scope.qrad.targetTemperature_css = 95;            }
                if ($scope.qrad.targetTemperature == max_temp - step)
                    $scope.qrad.targetTemperature_css = 90;

                $scope.humidity = $scope.qrad.sensors.humidityPct;
                $scope.co2 = ($scope.qrad.sensors.cO2LevelPermil - userConfig.minCO2) / userConfig.maxCO2 * 100;
                $scope.noise = ($scope.qrad.sensors.noisedB - userConfig.minDb) / userConfig.maxDb * 100;
        }
        
        Qrads.refreshWatch($scope);

        var refreshQrad = function () {
            Qrads.get($stateParams.id, initScopeForQrad);
        };
        var interval = parseInt(userConfig.detailsRefreshInterval) * 1000;
        if (isNaN(interval))
            interval = 3000;
        refreshQrad();
        setInterval(refreshQrad, interval, $scope);

        $scope.time_html = document.getElementById("time");

        var days = ["Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"];
        var months = ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Aout", "Septembre", "Octobre", "Novembre", "Décembre"];
        var displayTime = function DisplayTime($scope) {
            var currentTime = new Date();
            var hours = currentTime.getHours();
            hours = hours < 10 ? "0" + hours : hours;
            var minutes = currentTime.getMinutes();
            minutes = minutes < 10 ? "0" + minutes : minutes;
            var seconds = currentTime.getSeconds();
            seconds = seconds < 10 ? "0" + seconds : seconds;


            var day = currentTime.getDay();
            day = days[day - 1];
            var month = currentTime.getMonth();
            month = months[month];
            var year = currentTime.getYear();
            year += 1900;
            var number = currentTime.getDate().valueOf();

            $scope.hours = hours;
            $scope.minutes = minutes;
            $scope.seconds = seconds;
            $scope.day = day;
            $scope.month = month;
            $scope.year = year;
            $scope.number = number;
            if (!$scope.$$phase)
                $scope.$apply();
        };

        /*
        var httprgetTemperature = function() {
            var data = {};
            data.targetTemperature = $scope.qrad.targetTemperature;
            console.log(data);
            $http.put(API.urlBase + '/qrads/' + $scope.qrad.id, JSON.stringify(data), AuthService.config())
                .then(function(response) {

                }, function(response) {
                    console.log(response);
                });
        };*/


        $scope.downTemperature = function() {
            if ($scope.qrad.targetTemperature > min_temp) {
                $scope.qrad.targetTemperature -= step;
                $scope.qrad.targetTemperature_css = ($scope.qrad.targetTemperature - min_temp) / (max_temp - min_temp) * 100;
                if ($scope.qrad.targetTemperature_css == 0)
                    $scope.qrad.targetTemperature_css = 5;
                if ($scope.qrad.targetTemperature == max_temp - step)
                    $scope.qrad.targetTemperature_css = 90;

                Qrads.updateTemperatureQrad($scope.qrad);
            }

        };

        $scope.upTemperature = function() {
            if ($scope.qrad.targetTemperature < max_temp) {
                $scope.qrad.targetTemperature += step;
                $scope.qrad.targetTemperature_css = ($scope.qrad.targetTemperature - min_temp) / (max_temp - min_temp) * 100;
                if ($scope.qrad.targetTemperature_css == 100)
                    $scope.qrad.targetTemperature_css = 95;
                if ($scope.qrad.targetTemperature == max_temp - step)
                    $scope.qrad.targetTemperature_css = 90;

                Qrads.updateTemperatureQrad($scope.qrad);
            }
        };

        /*var addNotification = function(id, value) {
            var query = '/test/toast?id=' + id + '&temp=' + value;
            $http.get(query).
                success(function(data, status, headers, config) {
                }).
                error(function(data, status, headers, config) {
                });
        };*/

        displayTime($scope);
        setInterval(displayTime, 1000, $scope);

    });