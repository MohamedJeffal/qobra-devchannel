function addHexColor(c1, c2) {
    var hexStr = (parseInt(c1, 16) + parseInt(c2, 16)).toString(16);
    while (hexStr.length < 6) { hexStr = '0' + hexStr; } // Zero pad.
    return hexStr;
};

function randomColor() {
    var letters = '0123456789ABCDEF'.split('');
    var color = '#';
    for (var i = 0; i < 6; i++ ) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

function componentToHex(c) {
    var hex = c.toString(16);
    return hex.length == 1 ? "0" + hex : hex;
}

function rgbToHex(r, g, b) {
    return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
}

function rgb32ToHex(rgb) {
    var r = rgb / 10000;
    var g = rgb / 100 - 10 * r;
    var b = rgb % 100;
    return rgbToHex(r, g, b);
}

angular.module('starter.services', ['ionic.utils'])
    .factory('Qrads', function($http, AuthService, API) {
        var qrads = [];
        
        var refreshLock = false;
        
        var refreshWatch = function($scope) {
            $scope.$watch(function() {
                return refreshLock;
            }, function watchCallback(newValue, oldValue) {
                console.log('refresh lock before: ' + oldValue);
                console.log('refresh lock after: ' + newValue);
            });
        }
        
        function initQrads() {
            for (var i = 0; i < qrads.length; ++i) {
                qrads[i].color = randomColor();
                qrads[i].endColor = randomColor();
                //qrads[i].color = "#" + parseInt(qrads[i].data.colors.from).toString(16);
                //qrads[i].endColor = "#" + parseInt(qrads[i].data.colors.to).toString(16);
                
                var opacity = 0.5;
                var pointerEvents = 'none';
                
                if (qrads[i].available) {
                    opacity = 1;
                    pointerEvents = 'auto';    
                }
                
                qrads[i].opacity = opacity;
                qrads[i].pointerEvents = pointerEvents;
            }
        }

        var all = function(callback) {
            if (AuthService.isAuthenticated()) {
                $http.get(API.urlBase + '/qrads', AuthService.config())
                    .then(function (response) {
                        qrads = response.data;
                        initQrads();
                        callback(qrads);
                    }, function(response) {
                        //Print error
                        //Log.d(AuthService.config());
                        console.log(response);
                        callback([]);
                    });
            }
            return [];
        };

        var get = function(id, callback) {
            if (!refreshLock) {
                $http.get(API.urlBase + '/qrads/' + id)
                .then(function(response) {
                    console.log(JSON.stringify(response.data));
                    response.data.color = "#" + parseInt(response.data.colors.from).toString(16);
                    response.data.endColor = "#" + parseInt(response.data.colors.to).toString(16);
                    if (response.data.color == '#0') {
                        response.data.color = '#ffffff';
                        response.data.endColor = '#ffffff';
                    }
                    callback(response.data);
                }, function(response) {
                    callback(null);
                });
            }
            
        };

        function getInt32Colors(qrad) {
            var colors = {};
            colors.from = parseInt(qrad.color.substring(1), 16);
            colors.to = parseInt(qrad.endColor.substring(1), 16);
            return colors;
        }

        var updateInfoQrad = function(qrad) {
            var data = {};
            data.friendlyName = qrad.friendlyName;
            var colors = getInt32Colors(qrad);
            data.colors = colors;
            console.log(JSON.stringify(data));
            $http.put(API.urlBase + '/qrads/' + qrad.id, JSON.stringify(data), AuthService.config())
                .then(function(response) {

                }, function(response) {
                    console.log(response);
                });
        }

        var updateTemperatureQrad = function(qrad) {
            refreshLock = true;
            var data = {};
            data.targetTemperature = qrad.targetTemperature;
            console.log(data);
            $http.put(API.urlBase + '/qrads/' + qrad.id, JSON.stringify(data), AuthService.config())
                .then(function(response) {
                    refreshLock = false;
                }, function(response) {
                    console.log(response);
                });
        };

        return {
            all: all,
            get: get,
            clear: function() { qrads = []; },
            updateInfoQrad: updateInfoQrad,
            updateTemperatureQrad: updateTemperatureQrad,
            refreshWatch: refreshWatch
        };
    });