angular.module('starter.services')
    .factory('AuthService', function($q, $http, $localstorage, USER_ROLES, API) {
        var config = '';

        $http.defaults.useXDomain = true;

        var username = '';
        var firstname = '';
        var lastname = '';
        var role = '';
        var userConfig = {};
        var authToken ;

        function loadUserCredentials() {
            //Call the API to get a token
            var user = $localstorage.getObject('user');
            if (user) {
                useCredentials(user);
                config =  {
                        headers: {
                            'authorization': authToken
                        }
                    };
                if (config != {} && authToken && authToken != '')
                    loadUserConfiguration();
            }
        }

        function storeUserCredentials(user) {
            useCredentials(user);
            $localstorage.setObject('user', user);

            loadUserConfiguration();
            $localstorage.setObject('user_config', userConfig);
        }

        function loadUserConfiguration() {
            //Log.d("Config: " + config);
            $http.get(API.urlBase + '/user/config', config)
                .then(function (response) {
                    userConfig = response.data;
                }, function(response) {
                    //Print error
                    console.log("Could not load user config");
                });
        }

        function useCredentials(user) {
            if (user)
            {
                username = user.username;
                firstname = user.firstname;
                lastname = user.lastname;
                authToken = user.authToken;
                role = user.role;

                $http.defaults.headers.common['authorization'] = authToken;
            }
        }

        function destroyUserCredentials() {
            authToken = undefined;
            username = '';
            firstname = '';
            lastname = '';
            role = '';
            config = '';
            userConfig = {};
            $http.defaults.headers.common['authorization'] = undefined;

            $localstorage.remove('user');
        }

        var login = function(email, pw) {
            return $q(function(resolve, reject) {
                //Call the API to know if the user exists or not
                $http.post(API.urlBase + '/user/login', { 'email': email, 'password': pw}).
                    then(function(response) {
                        config =  {
                            headers: {
                                'authorization': response.data.id
                            }
                        };

                        var user = {};
                        user.username = email;
                        user.firstname = 'Vincent';
                        user.lastname = 'Molinié';
                        user.role = USER_ROLES.public;
                        user.authToken = response.data.id;

                        storeUserCredentials(user);
                        loadUserConfiguration();
                        resolve('Login success.');


                    }, function(response) {
                        // error
                        alert("Invalid Credentials");
                        reject('Login Failed.');
                    });
            });
        };

        var logout = function() {
            $http.get(API.urlBase + '/user/logout', config);
            destroyUserCredentials();
        };

        var isAuthenticated = function() {
          return username && username != '' && authToken && authToken != '';
        };

        var isAuthorized = function(authorizedRoles) {
            if (!angular.isArray(authorizedRoles)) {
                authorizedRoles = [authorizedRoles];
            }
            return (isAuthenticated && authorizedRoles.indexOf(role) !== -1);
        };

        var changePassword = function(password, new_password) {
            return $q(function (resolve, reject) {
               //Send to the API
                var success = true;
                if (success) {
                    resolve('Password changed.');
                } else {
                    reject('Error wrong password.');
                }
            });
        };

        var setUsername = function(newUsername) {
            username = newUsername;
        };
        var setFirstname = function (newFirstname) {
            firstname = newFirstname;
        };
        var setLastname = function (newLastname) {
            lastname = newLastname;
        };

        /**
        ** Send the current opened url to the native layer
        ** Called on page change
        */
        var setCurrentUrl = function(previousUrl) {
            $http.post("nativelavyer.io/history?previous=" + previousUrl)
                .then(function(response) {
                    console.log(response.data.result);
                }, function(response) {
                });
        }

        loadUserCredentials();

        return {
            login: login,
            logout: logout,
            isAuthorized: isAuthorized,
            isAuthenticated: isAuthenticated,

            username: function() {return username;},
            firstname: function() {return firstname;},
            lastname: function() {return lastname;},
            config: function() {return config;},
            userConfig: function() {return userConfig;},

            setUsername: setUsername,
            setFirstname: setFirstname,
            setLastname: setLastname,

            changePassword: changePassword,

            role: function() {return role;},

            setCurrentUrl: setCurrentUrl
        };
    });