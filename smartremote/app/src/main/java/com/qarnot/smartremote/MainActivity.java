package com.qarnot.smartremote;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebBackForwardList;
import android.webkit.WebHistoryItem;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String TAG = "MainActivity";

    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private WebView webView;

    private ArrayList<String> history;

    StringBuilder sb = new StringBuilder();

    private final Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        history = new ArrayList<>();

        onCreateCanvasPart();

        //onCreateNotificationPart();
    }

    private void onCreateNotificationPart()
    {
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                // TODO if a progress is suppose to be shown them do it here
            }
        };

        if (checkPlayServices()) {
            // Start IntentService to register this application with GCM.
            Intent intent = new Intent(this, RegistrationIntentService.class);
            // Uncomment only when token from RegistrationIntentSevice.java is uncomment
            //startService(intent);
        }
    }

    private void onCreateCanvasPart()
    {
        webView = (WebView) findViewById(R.id.webView);

        final WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);
        webSettings.setAllowFileAccessFromFileURLs(true);
        webSettings.setAllowUniversalAccessFromFileURLs(true);

        webView.setWebViewClient(new WebViewClient() {
            @Override
            @SuppressWarnings("deprecation")
            public WebResourceResponse shouldInterceptRequest(WebView view, String url) {

                // Handling the setCurrentUrl request (authService.js)
                if (url.contains("/history")) {
                    return new WebResourceResponse("text/json", "utf-8",
                            jsonToInputStream(previousUrlStore(url)));
                }


                return null;
            }
        });

        webView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    WebView webView = (WebView) v;

                    switch (keyCode) {
                        case KeyEvent.KEYCODE_BACK:
                            if (history.size() > 1) {
                                history.remove(history.size() - 1);

                                if (history.size() > 0) {
                                    String pushState = "javascript:window.history.pushState(null, null, 'file:///android_asset/www/index.html#" + history.get(history.size() - 1) + "');";
                                    webView.loadUrl(pushState);
                                }
                            }
                            return true;
                    }
                }

                return false;
            }
        });

        try {
            String html = readAssetFile("www/index.html");
            webView.loadDataWithBaseURL("file:///android_asset/www/index.html", html, "text/html", "UTF-8", "file:///android_asset/www/index.html#/home");
        } catch (IOException e) {
        }
    }



    @Override
    protected void onResume() {
        super.onResume();
        /*LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(QuickstartPreferences.REGISTRATION_COMPLETE));*/
    }

    @Override
    protected void onPause() {
        //LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    private String previousUrlStore(String url) {
        String isAddedString = "false";

        Uri uri = Uri.parse(url);
        String previousUrl = uri.getQueryParameter("previous");


        if ((history.size() > 1 && previousUrl.equals(history.get(history.size() - 2).toLowerCase())))
            history.remove(history.size() - 1);

        if (history.size() == 0
                || ((history.size() > 1 && !previousUrl.equals(history.get(history.size() - 2).toLowerCase()))
                && !previousUrl.equals(history.get(history.size() - 1).toLowerCase()))
                || ((history.size() == 1 && !previousUrl.equals(history.get(history.size() - 1).toLowerCase()))))
        {
            history.add(previousUrl.toLowerCase());
            isAddedString = "false";
        }

        String result = "{\"result\": " + isAddedString + "}";

        return result;
    }

    private InputStream jsonToInputStream(String json) {
        InputStream input = null;
        try {
            input = new ByteArrayInputStream(json.getBytes("UTF8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return input;
    }

    private String readAssetFile(String fileName) throws IOException {
        StringBuilder buffer = new StringBuilder();
        InputStream fileInputStream = getAssets().open(fileName);
        BufferedReader bufferReader = new BufferedReader(new InputStreamReader(fileInputStream, "UTF-8"));
        String str;

        while ((str=bufferReader.readLine()) != null) {
            buffer.append(str);
        }
        fileInputStream.close();

        return buffer.toString();
    }
}
