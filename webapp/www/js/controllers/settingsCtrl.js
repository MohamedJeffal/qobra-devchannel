angular.module('starter.controllers')

	.controller('SettingsCtrl', function($scope, $state, $http, $ionicPopup, AuthService) {
        var settings = {};
        settings.username = AuthService.username();
        settings.firstname = AuthService.firstname();
        settings.lastname = AuthService.lastname();

        $scope.logout = function() {
            AuthService.logout();
            $state.go('login');
        };

        $scope.saveSettings = function(settings) {
            AuthService.setUsername(settings.username);
            AuthService.setFirstname(settings.firstname);
            AuthService.setLastname(settings.lastname);

            if (settings.password != undefined
                && settings.new_password != undefined
                && settings.confirm_new_password != undefined) {

                if (settings.new_password != settings.confirm_new_password) {
                    //Error not same password
                } else {
                    //Everything seems okey
                    //Send the request
                    AuthService.changePassword(settings.password, settings.new_password);
                }
            } else if (settings.password != undefined
                || settings.new_password != undefined
                || settings.confirm_new_password != undefined) {
                //Error all input are not completed

            }

            settings.password = undefined;
            settings.new_password = undefined;
            settings.confirm_new_password = undefined;
        }
    });