angular.module('starter.controllers')

    .controller('QradCtrl', function($scope, $stateParams, $state, $http, Qrads, AuthService, API) {
        var min_temp = 17;
        var max_temp = 26;
        function initScopeForQrad(Qrad) {
            if (Qrad == null) {
                $state.go('home');
            } else {
                console.log(Qrad);
                $scope.qrad = Qrad;
                $scope.Temp_Float = Math.round(($scope.qrad.sensors.ambientTemperature % 1) * 10);
                $scope.Temp_Int = Math.floor($scope.qrad.sensors.ambientTemperature);

                $scope.temp_percent = ($scope.Temp_Int - min_temp) / (max_temp - min_temp) * 100;
                //$scope.qrad.targetTemperature = $scope.Temp_Int;
                $scope.qrad.targetTemperature_css = ($scope.qrad.targetTemperature - min_temp) / (max_temp - min_temp) * 100;

                if ($scope.qrad.targetTemperature_css == 0)
                    $scope.qrad.targetTemperature_css = 5;
                if ($scope.qrad.targetTemperature_css == 100)
                    $scope.qrad.targetTemperature_css = 95;            }
                if ($scope.qrad.targetTemperature == max_temp - 0.5)
                    $scope.qrad.targetTemperature_css = 90;

                $scope.humidity = $scope.qrad.sensors.humidityPct;
                $scope.co2 = $scope.qrad.sensors.cO2LevelPermil / 10;
                $scope.noise = $scope.qrad.sensors.noisedB / 70 * 100;
        }

        Qrads.get($stateParams.id, initScopeForQrad);

        $scope.time_html = document.getElementById("time");

        var days = ["Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"];
        var months = ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Aout", "Septembre", "Octobre", "Novembre", "Décembre"];
        var displayTime = function DisplayTime($scope) {
            var currentTime = new Date();
            var hours = currentTime.getHours();
            hours = hours < 10 ? "0" + hours : hours;
            var minutes = currentTime.getMinutes();
            minutes = minutes < 10 ? "0" + minutes : minutes;
            var seconds = currentTime.getSeconds();
            seconds = seconds < 10 ? "0" + seconds : seconds;


            var day = currentTime.getDay();
            day = days[day - 1];
            var month = currentTime.getMonth();
            month = months[month];
            var year = currentTime.getYear();
            year += 1900;
            var number = currentTime.getDate().valueOf();

            $scope.hours = hours;
            $scope.minutes = minutes;
            $scope.seconds = seconds;
            $scope.day = day;
            $scope.month = month;
            $scope.year = year;
            $scope.number = number;
            if (!$scope.$$phase)
                $scope.$apply();
        };

        var updateTargetTemperature = function() {
            var data = {};
            data.targetTemperature = $scope.qrad.targetTemperature;
            console.log(data);
            $http.put(API.urlBase + '/qrads/' + $scope.qrad.id, JSON.stringify(data), AuthService.config())
                .then(function(response) {

                }, function(response) {
                    console.log(response);
                });
        };


        $scope.downTemperature = function() {
            if ($scope.qrad.targetTemperature > min_temp) {
                $scope.qrad.targetTemperature -= 0.5;
                $scope.qrad.targetTemperature_css = ($scope.qrad.targetTemperature - min_temp) / (max_temp - min_temp) * 100;
                if ($scope.qrad.targetTemperature_css == 0)
                    $scope.qrad.targetTemperature_css = 5;
                if ($scope.qrad.targetTemperature == max_temp - 0.5)
                    $scope.qrad.targetTemperature_css = 90;

                updateTargetTemperature();
            }

        };

        $scope.upTemperature = function() {
            if ($scope.qrad.targetTemperature < max_temp) {
                $scope.qrad.targetTemperature += 0.5;
                $scope.qrad.targetTemperature_css = ($scope.qrad.targetTemperature - min_temp) / (max_temp - min_temp) * 100;
                if ($scope.qrad.targetTemperature_css == 100)
                    $scope.qrad.targetTemperature_css = 95;
                if ($scope.qrad.targetTemperature == max_temp - 0.5)
                    $scope.qrad.targetTemperature_css = 90;

                updateTargetTemperature();
            }
        };

        var addNotification = function(id, value) {
            var query = '/test/toast?id=' + id + '&temp=' + value;
            $http.get(query).
                success(function(data, status, headers, config) {
                }).
                error(function(data, status, headers, config) {
                });
        };

        displayTime($scope);
        setInterval(displayTime, 1000, $scope);

    });