angular.module('starter.controllers')
	.controller('HomeCtrl', function($scope, $state, AuthService, Qrads) {
        if (!AuthService.isAuthenticated) {
            $state.go('login');
        }

        function setQrads(response) {
            $scope.qrads = response;
        }
        $scope.qrads = [];
        Qrads.all(setQrads);

        setInterval(Qrads.all(setQrads()), 5000);


        /*$scope.DisplayQrad = function($scope, id) {
         var content = document.getElementById('content');
         content.clear();
         var dateTimeView = document.createElement('ul');
         dateTimeView.setAttribute('class', 'date_time');
         var timeView = document.createElement('li');
         timeView.setAttribute('class', 'time');
         var dateDayView = document.createElement('li');
         var dayView = document.createElement('strong');
         var dateView = document.createElement('strong');

         };*/

    });