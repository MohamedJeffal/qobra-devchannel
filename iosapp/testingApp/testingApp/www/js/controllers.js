angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});
  
  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
})
.controller('HomeCtrl', function($scope, Qrads) {
    $scope.qrads = Qrads.all();


            /*$scope.DisplayQrad = function($scope, id) {
        var content = document.getElementById('content');
        content.clear();
        var dateTimeView = document.createElement('ul');
        dateTimeView.setAttribute('class', 'date_time');
        var timeView = document.createElement('li');
        timeView.setAttribute('class', 'time');
        var dateDayView = document.createElement('li');
        var dayView = document.createElement('strong');
        var dateView = document.createElement('strong');

    };*/

})
.controller('QradCtrl', function($scope, $stateParams, $http, Qrads) {
    $scope.qrad = Qrads.get($stateParams.id);
    $scope.time_html = document.getElementById("time");
    $scope.Temp_Float = $scope.qrad.temp % 1;
    $scope.Temp_Int = $scope.qrad.temp - $scope.Temp_Float;

    var min_temp = 18;
    var max_temp = 40;
    $scope.temp_percent = ($scope.Temp_Int - min_temp) / (max_temp - min_temp) * 100;
    console.log($scope.temp_percent);
    $scope.temp_indic = $scope.Temp_Int;
    $scope.temp_indic_css = ($scope.temp_indic - min_temp) / (max_temp - min_temp) * 100;

    var days = ["Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"];
    var months = ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Aout", "Septembre", "Octobre", "Novembre", "Décembre"];
    var displayTime = function DisplayTime($scope) {
            var currentTime = new Date();
            var hours = currentTime.getHours();
            hours = hours < 10 ? "0" + hours : hours;
            var minutes = currentTime.getMinutes();
            minutes = minutes < 10 ? "0" + minutes : minutes;
            var seconds = currentTime.getSeconds();
            seconds = seconds < 10 ? "0" + seconds : seconds;


            var day = currentTime.getDay();
            day = days[day - 1];
            var month = currentTime.getMonth();
            month = months[month];
            var year = currentTime.getYear();
            year += 1900;
            var number = currentTime.getDate().valueOf();

            $scope.hours = hours;
            $scope.minutes = minutes;
            $scope.seconds = seconds;
            $scope.day = day;
            $scope.month = month;
            $scope.year = year;
            $scope.number = number;
            if (!$scope.$$phase)
                $scope.$apply();
    };
    var DownTemperature = function() {
        if ($scope.temp_indic > min_temp) {
            $scope.temp_indic -= 1;
            $scope.temp_indic_css = ($scope.temp_indic - min_temp) / (max_temp - min_temp) * 100;
            if ($scope.temp_indic_css == 0)
                $scope.temp_indic_css = 5;
        }

        addNotification($stateParams.id, $scope.temp_indic);
    };
    var UpTemperature = function() {
        console.log("Up");
        if ($scope.temp_indic < max_temp) {
            $scope.temp_indic += 1;
            $scope.temp_indic_css = ($scope.temp_indic - min_temp) / (max_temp - min_temp) * 100;
            if ($scope.temp_indic_css == 100)
                $scope.temp_indic_css = 95;
        }

        addNotification($stateParams.id, $scope.temp_indic);
    };

    var addNotification = function(id, value) {
        var query = '/test/toast?id=' + id + '&temp=' + value;
        $http.get(query).
          success(function(data, status, headers, config) {
            console.log(data.name);
          }).
          error(function(data, status, headers, config) {
          });
    };

    document.getElementById("down").addEventListener('click', DownTemperature);
    document.getElementById("up").addEventListener('click', UpTemperature);

    displayTime($scope);
    setInterval(displayTime, 1000, $scope);
});
