/**
 * Created by vince on 6/11/15.
 */
function addHexColor(c1, c2) {
    var hexStr = (parseInt(c1, 16) + parseInt(c2, 16)).toString(16);
    while (hexStr.length < 6) { hexStr = '0' + hexStr; } // Zero pad.
    return hexStr;
};

angular.module('starter.services', [])
.factory('Qrads', function() {
        var qrads = [
            {
                id:                   1,
                name:           'Salon',
                color:            'E0081E',
                endColor:         addHexColor('E0081E', '1FE85'),
                type:           'salon',
                temp:                 25
            },
            {
                id:                2,
                name: 'Chambre Paul',
                color:      '00B3E9',
                endColor:    addHexColor('00B3E9', '1FE85'),
                type:      'chambre',
                temp:              22
            },
            {
                id:                   3,
                name: 'Chambre Parents',
                color:           'F19319',
                endColor:        addHexColor('F19319', '1FE85'),
                type:         'chambre',
                temp:                 21
            },
            {
                id:                 4,
                name: 'Chambre Vince',
                color:        '90BD30',
                endColor:      addHexColor('90BD30', '1FE85'),
                type:       'chambre',
                temp:               24
            },
            {
                id:                   5,
                name: 'Chambre Justine',
                color:           'F19319',
                endColor:      addHexColor('F19319', '1FE85'),
                type:         'chambre',
                temp:                 23
            }
        ];

        return {
            all: function() { return qrads; },
            get: function(qradId) {
                for (var i = 0; i < qrads.length; ++i) {
                    if (qrads[i].id == qradId)
                        return qrads[i];
                }
                return null;
            }

        };
    });