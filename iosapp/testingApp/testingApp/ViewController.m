//
//  ViewController.m
//  testingApp
//
//  Created by Guest User on 03/09/15.
//  Copyright (c) 2015 Guest User. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UIWebView *webview;

@end

@implementation ViewController

// An example viewDidLoad
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loadWebView];
}

// The example loader
//
// Assumes you have an IBOutlet for the UIWebView defined:  @property (strong, nonatomic) UIWebView *wv;
- (void)loadWebView {
    NSString *htmlPath = [[NSBundle mainBundle] pathForResource:@"index" ofType:@"html" inDirectory:@"www"];
    
    NSURL *localHTMLURL = [NSURL fileURLWithPath:htmlPath isDirectory:NO];
    
    
    NSURLRequest *request = [NSURLRequest requestWithURL:localHTMLURL
                                    cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                         timeoutInterval:60];
    

    [self.webview loadRequest:request];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
